#!/bin/bash

rm gpx/*_reduced.gpx
for orig in $(ls gpx/*.gpx); do
    echo "Processing $orig"
    dest=$(echo $orig | sed 's/\.gpx/_reduced.gpx/')

    # Used with strava-local-heatmap
    gpsbabel -i gpx -f $orig -x position,distance=20m -o gpx -F $dest

    # Used with strava-local-heatmap (For showing road junction)
    #gpsbabel -i gpx -f $orig -x simplify,crosstrack,error=0.020k -o gpx -F $dest

done