#!/bin/python
# -*- coding: UTF-8 -*-

import os
import csv
import json
import requests
from pathlib import Path
from datetime import datetime

def download_gpx(fileid):
    url = 'https://www.openstreetmap.org/trace/%(fileid)s/data' % locals()
    
    print ("Download %(url)s" % locals())
    r = requests.get(url, allow_redirects=True)
    open('gpx/%(fileid)s.gpx' % locals(), 'wb').write(r.content)

def readcsv(filename):	
    with open(filename, 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        lines = []
        for row in reader:
            lines.append (row)
        
    return lines


lines = readcsv('gpx/gpxlist.csv' % locals())
for row in lines:
    id,author,date,timestamp = row
    try:
        fileid = int(id)
    
        fields = {
            'fileid': fileid,
            'author': author,
            'date': date,
            'timestamp': timestamp
        }
        
        gpxname = 'gpx/%(fileid)s.gpx' % locals()
        jsonname = 'gpx/%(fileid)s.json' % locals()
        if not os.path.exists(gpxname):
            download_gpx(fileid)
            with open(jsonname, 'w') as f:
                json.dump(fields, f, ensure_ascii=False,)

            # with open('%(fileid)s.json') as f:
            #     data = json.load(f)
    except ValueError:
        pass